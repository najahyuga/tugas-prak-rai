-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 01, 2022 at 09:01 AM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 8.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_pendataanmhs`
--

-- --------------------------------------------------------

--
-- Table structure for table `formm`
--

CREATE TABLE `formm` (
  `id` int(11) NOT NULL,
  `nim` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `agama` varchar(20) NOT NULL,
  `notelpon` int(11) NOT NULL,
  `tempatlahir` varchar(100) NOT NULL,
  `tgllahir` date NOT NULL,
  `jeniskelamin` varchar(20) NOT NULL,
  `anakke` int(11) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `jurusan` varchar(50) NOT NULL,
  `hobi` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `formm`
--

INSERT INTO `formm` (`id`, `nim`, `nama`, `agama`, `notelpon`, `tempatlahir`, `tgllahir`, `jeniskelamin`, `anakke`, `alamat`, `jurusan`, `hobi`) VALUES
(1, 1201202001, 'kahf', 'Islam', 2147483647, 'Surabaya', '2001-12-23', '', 1, 'surabaya', 'rpl', ''),
(2, 2147483647, 'Kahfi', 'Islam', 8123123, 'Surabaya', '2022-06-30', 'Laki - laki', 1, 'surabaya', 'se', 'membaca'),
(3, 2147483647, 'Data', 'Islam', 123213, 'Banda Aceh', '2022-06-26', 'lakilaki', 1, 'banda', 'data sains', 'Hiking'),
(8, 321, 'ada', 'Islam', 12345, 'Denpasar', '2022-05-18', 'lakilaki', 1, 'ada', 'ada', 'membaca');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `formm`
--
ALTER TABLE `formm`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `formm`
--
ALTER TABLE `formm`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
