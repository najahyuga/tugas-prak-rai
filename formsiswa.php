<html>
	<head>
		<title>FORM PENDATAAN MAHASISWA BARU</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>		
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
		<style type="text/css">
			body { background-color:White; }
		</style>
	</head>
<body>
	<div class="container">		
		<form method="POST" action="proses.php" >

			<br><div class="input-group mb-3 input-group-lg">
				<div class="input-group-prepend">
					<span class="input-group-text">NIM&emsp;&emsp;</span>
				</div>
				<input type="text" name="nim" class="form-control">
			</div>

			<div class="input-group mb-3 input-group-lg">
				<div class="input-group-prepend">
					<span class="input-group-text">Nama&emsp;&emsp;</span>
				</div>
					<input type="text" name="nama" class="form-control">
			</div>
				
			<div class="form-group">
				<label for="sel1">Agama	:</label> 
				<select name="agama" class="form-control">	
					<option value="Islam">Islam</option>
					<option value="Kristen">Kristen</option>
					<option value="Katolik">Katolik</option>
					<option value="Hindu">Hindu</option>
					<option value="Budha">Budha</option>
				</select><br></h3>
			</div>
			
				<div class="input-group mb-3 input-group-lg">
				<div class="input-group-prepend">
				<span class="input-group-text">No Telp:&emsp;</span>
				</div>
					<input type="number" name="notelpon" class="form-control">
				</div>
			
			<div class="form-group">
				<label for="sel1">Tempat Lahir:</label> 
				<select name="tempatlahir" class="form-control">
					<option value="Banda Aceh">Banda Aceh</option>
					<option value="Jambi">Jambi</option>
					<option value="Bengkulu">Bengkulu</option>
					<option value="Pangkalpinang">Pangkalpinang</option>
					<option value="Tanjungpinang">Tanjungpinang</option>
					<option value="Medan">Medan</option>
					<option value="Pekan Baru">Pekan Baru</option>
					<option value="Palembang">Palembang</option>
					<option value="Bandar Lampung">Bandar Lampung</option>
					<option value="Padang">Padang </option>
				
					<option value="Denpasar">Denpasar</option>
					<option value="Serang">Serang</option>
					<option value="Jakarta">Jakarta</option>
					<option value="Bandung">Bandung</option>
					<option value="Surabaya">Surabaya</option>
					<option value="Yogyakarta">Yogyakarta</option>
					<option value="Semarang">Semarang</option>
			
					<option value="Mataram">Mataram</option>
					<option value="Kupang">Kupang</option>
			
					<option value="Tanjung Selor">Tanjung Selor</option>
					<option value="Pontianak">Pontianak</option>
					<option value="Palangkaraya">Palangkaraya</option>
					<option value="Banjarmasin">Banjarmasin</option>
					<option value="Samarinda">Samarinda</option>
			
					<option value="Manado">Manado</option>
					<option value="Kota Mamuju">Kota Mamuju</option>
					<option value="Palu">Palu</option>
					<option value="Kendari">Kendari</option>
					<option value="Makassar">Makassar</option>
					<option value="Gorontalo">Gorontalo</option>
			
					<option value="Ambon">Ambon</option>
					<option value="Ternate">Ternate</option>
					<option value="Manokwari">Manokwari</option>
					<option value="Jayapura">Jayapura</option>
		
				</select></h3>
			</div>
		
			<h5>Tanggal Lahir &emsp;: <input type="date" name="tgllahir" title="dd-mm-yyyy"><br></h5><br>
			
			<div class="form-group" name="jeniskelamin">
				<label for="sel1">Jenis Kelamin	:</label>&emsp; 
				<label class="radio-inline">
					<input type="radio" name="jeniskelamin" value="lakilaki" checked>Laki - laki
				</label>
				<label class="radio-inline">
					<input type="radio" name="jeniskelamin" value="perempuan">Perempuan
				</label>
			</div>
			
			<div class="form-group">
				<label for="sel1">Anak Ke	:</label> 
				<select name="anakke" class="form-control">	
					<option value="1">1</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
					<option value="7">7</option>
					<option value="8">8</option>
					<option value="9">9</option>
					<option value="10">10</option>
				</select><br>
			</div>
		
			<div class="form-group">
				<label for="comment">Alamat:</label>
				<textarea class="form-control" rows="5" name="alamat" id="alamat"></textarea>
			</div>
		
			<div class="input-group mb-3 input-group-lg">
				<div class="input-group-prepend">
					<span class="input-group-text">Jurusan&emsp;&emsp;</span>
				</div>
					<input type="text" name="jurusan" class="form-control">
			</div>
			
			<div class="form-group">
				<label for="comment">Hobi:</label>
				<div class="checkbox">
					<label><input type="checkbox" name="hobi" value="membaca">&emsp; Membaca</label>
				</div>
				<div class="checkbox">
					<label><input type="checkbox" name="hobi" value="olahraga">&emsp; Olahraga</label>
				</div>
				<div class="checkbox">
					<label><input type="checkbox" name="hobi" value="berenang">&emsp; Berenang</label>
				</div>
				<div class="checkbox">
					<label><input type="checkbox" name="hobi" value="hiking">&emsp; Hiking</label>
				</div>
				<div class="checkbox">
					<label><input type="checkbox" name="hobi" value="lainya">&emsp; Lainnya</label>
				</div>
			</div>
		
			<button type="submit" name ="simpan" value="simpan" button type ="button" class="btn btn-primary">SIMPAN </button>
		</form> 
	</div>
</body>	

</html>